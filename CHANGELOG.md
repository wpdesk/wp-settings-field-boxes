## [2.2.4] - 2023-06-15
### Fixed
- assets files sizes

## [2.2.3] - 2023-05-18
### Fixed
- packer box settings

## [2.2.1] - 2023-04-18
### Fixed
- return type for jsonSerialize method

## [2.2.0] - 2022-08-30
### Added
- de_DE translations

## [2.1.1] - 2022-05-16
### Fixed
- gitlab CI

## [2.1.0] - 2022-05-16
### Changes
- allow wpdesk-packer v3

## [2.0.7] - 2021-01-12
### Fixed
- input field change action

## [2.0.6] - 2021-01-11
### Fixed
- field description

## [2.0.5] - 2020-05-21
### Added
- custom box dimensions in custom box name

## [2.0.4] - 2020-02-04
### Fixed
- box IDs should be compared as strings

## [2.0.3] - 2020-01-28
### Fixed
- find built in box by code

## [2.0.2] - 2020-01-28
### Fixed
- Box names, codes and IDs

## [2.0.1] - 2020-01-23
### Fixed
- PackerBoxesFactory

## [2.0.0] - 2020-01-09
### Changed
- Packer 2.0 with name and is is used
- Assumption about build in boxes array keys in PackerBoxesFactory is dropped

## [1.0.6] - 2019-12-27
### Removed
- UPS packer no longer required
### Added
- Packer is required

## [1.0.5] - 2019-09-27
### Added
- import react in each component 

## [1.0.4] - 2019-09-24
### Fixed
- default box weight
- default box data from shipping service (ie. UPS) 

## [1.0.3] - 2019-09-24
### Fixed
- built in boxes weight and name 

## [1.0.2] - 2019-09-20
### Added
- padding in packer boxes factory 

## [1.0.1] - 2019-09-20
### Added
- create built in box from packer box 

## [1.0.0] - 2019-09-20
### Added
- init
