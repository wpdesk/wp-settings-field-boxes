import React, { Component } from 'react';

export default class BoxType extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            box: props.box,
            name: props.name,
            value: props.value,
            builtInBoxes: props.builtInBoxes,
            handleTypeChange: props.handleTypeChange
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let state = this.state;
        state.value = event.target.value;
        state.box.code = event.target.value;
        this.setState(state);
        this.state.handleTypeChange(event.target.value);
    }

    render () {

        let className = 'select ';

        let options = [];
        options.push({value: 'custom', label: 'Custom box'});
        this.state.builtInBoxes.forEach(function(box){
            options.push({value: box.code, label: box.name});
        });

        return (
            <select
                className={className}
                name={this.state.name}
                onChange={this.handleChange}
                value={this.state.value}
            >
                {
                    options.map(function (option) {
                        return <option key={option.value} value={option.value}>{option.label}</option>;
                    })
                }
            </select>
        )
    }
}
