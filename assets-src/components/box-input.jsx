import React, { Component } from 'react';

export default class BoxInput extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            box: props.box,
            field: props.field,
            type: props.type,
            name: props.name,
            value: props.value,
            readOnly: props.readOnly
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(props){
        this.setState({
            box: props.box,
            field: props.field,
            type: props.type,
            name: props.name,
            value: props.value,
            readOnly: props.readOnly
        });
    }

    handleChange(event) {
        let state = this.state;
        state.value = event.target.value;
        state.box[state.field] = event.target.value;
        this.setState(state);
    }

    render () {
        let className = 'input-text regular-input';

        if ( this.state.type === 'dimension' || this.state.type === 'weight' ) {
            className = 'wc_input_decimal input-text dimension';
        } else if ( this.state.type === 'weight' ) {
            className = 'wc_input_decimal input-text weight';
        }

        return (
            <input
                className={className}
                type="text"
                name={this.state.name}
                value={this.state.value}
                required="required"
                readOnly={this.state.readOnly}
                onChange={this.handleChange}
            />
        )
    }
}
