<?php

use WP_Mock\Tools\TestCase;
use WPDesk\Packer\Box\BoxImplementation;
use WpDesk\WooCommerce\ShippingMethod\BuiltInBox;
use WpDesk\WooCommerce\ShippingMethod\Labels;
use WpDesk\WooCommerce\ShippingMethod\SettingsField;

class Test_SettingsField extends TestCase {

    /**
     * Set up.
     */
    public function setUp() {
        \WP_Mock::setUp();
    }

    /**
     * Tear down.
     */
    public function tearDown() {
        \WP_Mock::tearDown();
    }

    /**
     * Test posted value to JSON.
     */
    public function test_get_field_posted_value_as_json() {

        $settings_field = new SettingsField( 'boxes' );

        $this->assertEquals( '[]', $settings_field->get_field_posted_value_as_json( null ), 'Null posted value should be empty array in JSON!' );

        $this->assertEquals( '{"name":"value"}', $settings_field->get_field_posted_value_as_json( array( 'name' => 'value' ) ), 'Posted array value should be array in JSON!' );

    }

    /**
     * Test render.
     */
    public function test_render() {

        $field_name = 'boxes';
        $settings_field = new SettingsField( $field_name );

        \WP_Mock::passthruFunction( 'wp_kses_post' );

        $json_values = '[{"name":"value"}]';

        $built_in_boxes_from_packer = array( '01' => new BoxImplementation( 100, 100, 100, 1, null, '01', '01', array( 'id' => '01' ) ) );

        $built_in_boxes = array();
        foreach ( $built_in_boxes_from_packer as $code => $built_in_box_from_packer ) {
            $built_in_boxes[] =  BuiltInBox::create_from_code_and_packer_box( $code, $built_in_box_from_packer );;
        }

        $this->expectOutputString('<tr valign="top">
    <th scope="row" class="titledesc">
        <label for="boxes">Field Title<span>Tooltip</span></label>
    </th>
    <td class="forminp">
        <fieldset
            class="settings-field-boxes"
            id="boxes_fieldset"
            data-value="' . $json_values . '"
            data-name="' . $field_name . '"
            data-builtinboxes="' . json_encode( array_values( $built_in_boxes ) ) . '"
            data-labels="' . esc_attr( json_encode( new Labels(), JSON_FORCE_OBJECT ) ) . '"
        >
        </fieldset>
    </td>
</tr>
');

        $settings_field->render(
            'Field Title',
            '<span>Tooltip</span>',
            $json_values,
            $built_in_boxes_from_packer,
            null
        );

    }

}
